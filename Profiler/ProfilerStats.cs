using System;
using System.Collections.Generic;
using RobinBird.Utilities.Runtime.Helper;
using Unity.Profiling;
using UnityEngine;
using ProfilerCounterOptions = Unity.Profiling.ProfilerCounterOptions;
using ProfilerMarkerDataUnit = Unity.Profiling.ProfilerMarkerDataUnit;

namespace RobinBird.Utilities.Unity.Profiler
{
	public class UnityProfilerTracking : IProfilerTrackingHandler
	{
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
		static void Init()
		{
			Debug.Log("Unregistering counters.");
			if (resetCounterActions != null)
			{
				foreach (var resetCounterAction in resetCounterActions)
				{
					resetCounterAction?.Invoke();
				}
				resetCounterActions.Clear();
			}
		}
		
		private static readonly List<Action> resetCounterActions = new(); 
		
		public (Func<TValueType> getter, Action<TValueType> setter) CreateCounterHandle<TValueType>(string name, string category, Runtime.Helper.ProfilerMarkerDataUnit unit,
			Runtime.Helper.ProfilerCounterOptions counterOptions) where TValueType : unmanaged
		{
			var profileCounter = new ProfilerCounterValue<TValueType>(new ProfilerCategory(category),
				name,
				(ProfilerMarkerDataUnit)unit,
				(ProfilerCounterOptions)counterOptions);
			resetCounterActions.Add(() => profileCounter.Value = default);

			return (() => profileCounter.Value,(value) =>
			{
				profileCounter.Value = value;
			});
		}
	}
}