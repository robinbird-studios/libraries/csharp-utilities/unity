using System;
using UnityEngine;

namespace RobinBird.Utilities.Unity.PropertyDrawerAttributes
{
	public class EnumIntPropertyDrawerAttribute : PropertyAttribute
	{
		public Type EnumType {get; private set;}

		public EnumIntPropertyDrawerAttribute(Type enumType)
		{
			EnumType = enumType;
		}
	}
}