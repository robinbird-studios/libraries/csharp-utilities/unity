namespace RobinBird.Utilities.Unity.PropertyDrawerAttributes
{
	public class ReadOnlyAttribute : AbstractPropertyDrawerAttribute
	{
		public ReadOnlyAttribute(string tooltip) : base(tooltip)
		{
		}
	}
}