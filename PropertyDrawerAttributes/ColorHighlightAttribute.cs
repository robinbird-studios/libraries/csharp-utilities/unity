using UnityEngine;

namespace RobinBird.Utilities.Unity.PropertyDrawerAttributes
{
	public class ColorHighlightAttribute : TalosPropertyAttribute
	{
		public Color HighlightColor;

		public ColorHighlightAttribute(float r, float g, float b, float a = 1)
		{
			HighlightColor = new Color(r, g, b, a);
		}
	}
}