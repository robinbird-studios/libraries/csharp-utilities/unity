using RobinBird.Logging.Runtime;

namespace RobinBird.Utilities.Unity.Helper
{
    using UnityEngine;

    /// <summary>
    ///     Applies the safe area to attached RectTransform. Attached RectTransform should be set to scale to parent bounds
    ///     Parent bounds should match the screen size
    /// </summary>
    public class SafeAreaHelper : MonoBehaviour
    {
	    /// <summary>
	    /// Applies this margin to all screens regardless if the device has a safe area or not.
	    /// With this solution we can design our interfaces to be flush to the safe area if one exists.
	    /// </summary>
	    public Vector4 NoSafeAreaMargin;

	    /// <summary>
	    /// The default safe area on the iPhone for the HomeBar (white bar at the bottom) is pretty big. We use this value to specify a custom one
	    /// </summary>
	    public float iOSHomeBarSafeAreaOverride = 10;

	    private static bool loggedScreenDimensions = false;

	    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
	    static void Init()
	    {
		    loggedScreenDimensions = false;
	    }

	    private void Awake()
        {
            ApplySafeArea();
        }

	    [ContextMenu("ApplySafeArea")]
        public void ApplySafeArea()
        {
            var rectTransform = GetComponent<RectTransform>();

            if (rectTransform == null)
            {
                return;
            }

            var screenResolution = new Rect(0, 0, Screen.width, Screen.height);
            Rect safeArea = Screen.safeArea;
            bool doesSafeAreaExist = screenResolution != safeArea;

            if (loggedScreenDimensions == false)
            {
	            Log.Info($"Got safe area: {safeArea}");
	            loggedScreenDimensions = true;
            }

            if (doesSafeAreaExist == false)
            {
	            // Adjust for edit time so we are flush with the screen
	            safeArea = screenResolution;
	            safeArea.xMin += NoSafeAreaMargin.x;
	            safeArea.yMin += NoSafeAreaMargin.y;
	            safeArea.xMax -= NoSafeAreaMargin.z;
	            safeArea.yMax -= NoSafeAreaMargin.w;
            }

            float xMin = safeArea.xMin;
            float xMax = safeArea.xMax - screenResolution.width;

            float yMin = safeArea.yMin;
            float yMax = safeArea.yMax - screenResolution.height;

            rectTransform.offsetMin = new Vector2(xMin, yMin);
            rectTransform.offsetMax = new Vector2(xMax, yMax);
        }
    }
}