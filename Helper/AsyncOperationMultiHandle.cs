using System;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace RobinBird.Utilities.Unity.Helper
{
	public class AsyncOperationMultiHandle
	{
		private readonly Action onCompleted;
		private int handleCounter;

		public AsyncOperationMultiHandle(Action onCompleted)
		{
			this.onCompleted = onCompleted;
		}
		
		public void AddHandle(AsyncOperationHandle handle)
		{
			handleCounter++;
			handle.Completed += OnCompleted;
		}

		private void OnCompleted(AsyncOperationHandle obj)
		{
			handleCounter--;
			if (handleCounter == 0)
			{
				onCompleted?.Invoke();
			}
		}
	}
}