using System.Reflection;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace RobinBird.Utilities.Unity.Helper.AddressableInternalHelpers
{
	public static class AddressableWebInitializer
	{
		public static AsyncOperationHandle<IResourceLocator> InitializeAddressablesSuffix(string settingsFileSuffix)
		{
			var addressableType = typeof(Addressables);
			var internalAddressableProp = addressableType.GetProperty("m_Addressables", BindingFlags.Static | BindingFlags.NonPublic);
			var internalAddressables = (AddressablesImpl)internalAddressableProp.GetValue(null, null);
			
			var settingsPath =
#if UNITY_EDITOR
				PlayerPrefs.GetString(Addressables.kAddressablesRuntimeDataPath, Addressables.RuntimePath + "/settings.json");
#else
                Addressables.RuntimePath + "/settings.json";
#endif

			return internalAddressables.InitializeAsync(Addressables.ResolveInternalId(settingsPath + settingsFileSuffix));
		}
	}
}