using UnityEngine;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Helper
{
	[ExecuteInEditMode]
	public class BackgroundScreenFitter : MonoBehaviour
	{
		[SerializeField]
		private RectTransform rectTransform;
		[SerializeField]
		private Image image;

		private void Start()
		{
			// Resize once on start
			AdjustSize();
		}

		private void Update()
		{
			AdjustSize();
		}

		private void OnValidate()
		{
			AdjustSize();
		}

		private void AdjustSize()
		{
			if (rectTransform == null || image == null || image.sprite == null)
				return;

			// The source sprite's aspect ratio (width / height)
			float spriteAspect = image.sprite.rect.width / image.sprite.rect.height;

			// The current screen's aspect ratio
			float screenAspect = (float)Screen.width / Screen.height;

			if (screenAspect < spriteAspect)
			{
				// Screen is relatively wider than the image:
				// Match the entire screen height, adjust width to keep aspect ratio
				float height = 1000 / screenAspect;  // or Screen.height if preferred
				float width = height * spriteAspect;
				rectTransform.sizeDelta = new Vector2(width, height);
			}
			else
			{
				// Screen is relatively taller than the image:
				// Match the entire screen width, adjust height to keep aspect ratio
				float width = 1000;  // or Screen.width if preferred
				float height = width / spriteAspect;
				rectTransform.sizeDelta = new Vector2(width, height);
			}
		}
	}

}