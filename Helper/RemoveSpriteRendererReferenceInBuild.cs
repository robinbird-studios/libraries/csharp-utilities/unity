using RobinBird.Utilities.Unity.Extensions;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Helper
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class RemoveSpriteRendererReferenceInBuild : MonoBehaviour
	{
#if UNITY_EDITOR
		// TODO: Does not work with prefabs! Use a Asset Label "CheckOnBuild" and search for that during build to destroy any GameObjects in prefabs before the build
		[UnityEditor.Callbacks.PostProcessScene]
		public static void PostProcessScene()
		{
			var removeImageInBuilds = FindObjectsOfType<RemoveSpriteRendererReferenceInBuild>();
			if (removeImageInBuilds == null)
				return;
			foreach (RemoveSpriteRendererReferenceInBuild obj in removeImageInBuilds)
			{
				if (obj.TryGetComponent<SpriteRenderer>(out var spriteRenderer))
				{
					spriteRenderer.sprite = null;
				}

				obj.Destroy();
			}
		}
#endif
	}
}