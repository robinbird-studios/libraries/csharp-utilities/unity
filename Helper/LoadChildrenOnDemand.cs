using System;
using System.Collections;
using System.Collections.Generic;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Unity.Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace RobinBird.Utilities.Unity.Helper
{
	/// <summary>
	/// Helper class to dynamically load views on demand when they are opened
	/// Reduces initial scene loading time and needed asset references.
	/// </summary>
	public class LoadChildrenOnDemand : MonoBehaviour
	{
		public AssetReferenceGameObject PrefabAssetReference;
		
		public event Action LoadComplete;

		[NonSerialized]
		public bool HasLoadedChildren;
		[NonSerialized]
		public bool IsLoading;

		public Vector3 RectTransformAnchoredPosition3d;

		public string GeneratedPrefabDirectoryGuid;

		public string AddressableGroupGuid;

		public void LoadView(Action onComplete)
		{
			if (HasLoadedChildren)
			{
				// Already loaded
				onComplete?.Invoke();
				return;
			}

			if (IsLoading)
			{
				Log.Warn("Loading already in progress");
				return;
			}
			
			if (PrefabAssetReference.RuntimeKeyIsValid() == false)
			{
				Log.Error("Could not load empty view reference.");
				onComplete?.Invoke();
				return;
			}

			IsLoading = true;
			PrefabAssetReference.ProcessResultWhenReady(handle =>
			{
				// int handleCount = handle.gameObject.transform.childCount;
				// for (int i = 0; i < handle.gameObject.transform.childCount; i++)
				// {
				// 	var prefabChild = handle.gameObject.transform.GetChild(i);
				// 	AsyncInstantiateOperation<GameObject> instantiateHandle = InstantiateAsync(prefabChild.gameObject, transform, transform.position, transform.rotation);
				// 	instantiateHandle.completed += HandleCompleted;
				// }
				//
				// void HandleCompleted(AsyncOperation obj)
				// {
				// 	handleCount--;
				// 	if (handleCount == 0)
				// 	{
				// 		HasLoadedChildren = true;
				// 		onComplete?.Invoke();
				// 	}
				// }
				
				AsyncInstantiateOperation<GameObject> instantiateHandle = InstantiateAsync(handle.gameObject, transform.parent);
				instantiateHandle.completed += operation =>
				{
					var op = (AsyncInstantiateOperation<GameObject>)operation;
					var go = op.Result[0];
					if (go.TryGetComponent(out RectTransform rectTransform))
					{
						rectTransform.anchoredPosition3D = RectTransformAnchoredPosition3d;
					}
					while (go.transform.childCount > 0)
					{
						Transform child = go.transform.GetChild(0);
						child.transform.SetParent(transform);
					}
					go.Destroy();
					HasLoadedChildren = true;
					IsLoading = false;
					LoadComplete?.Invoke();
					onComplete?.Invoke();
				};
			});
		}

		public IEnumerator LoadViewCoroutine()
		{
			bool isDone = false;
			LoadView(() =>
			{
				isDone = true;
			});
			while (isDone == false)
			{
				yield return null;
			}
		}
	}
}