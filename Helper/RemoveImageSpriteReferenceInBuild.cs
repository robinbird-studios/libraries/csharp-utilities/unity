using RobinBird.Utilities.Unity.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Helper
{
	[RequireComponent(typeof(Image))]
	public class RemoveImageSpriteReferenceInBuild : MonoBehaviour
	{
#if UNITY_EDITOR
		// TODO: Does not work with prefabs! Use a Asset Label "CheckOnBuild" and search for that during build to destroy any GameObjects in prefabs before the build
		[UnityEditor.Callbacks.PostProcessScene]
		public static void PostProcessScene()
		{
			var removeImageInBuilds = FindObjectsOfType<RemoveImageSpriteReferenceInBuild>();
			if (removeImageInBuilds == null)
				return;
			foreach (RemoveImageSpriteReferenceInBuild obj in removeImageInBuilds)
			{
				obj.RemoveSprite();
			}
		}

#endif
		public void RemoveSprite()
		{
			if (gameObject.TryGetComponent(out Image image))
			{
				image.sprite = null;
			}

			this.Destroy();
		}
	}
}