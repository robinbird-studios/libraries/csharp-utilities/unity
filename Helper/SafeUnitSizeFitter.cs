using System;
using UnityEngine;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Helper
{
	[ExecuteAlways]
	public class SafeUnitSizeFitter : MonoBehaviour
	{
		private RectTransform rectTransform;

		[SerializeField]
		private RectTransform aspectRect;

		private Canvas rootCanvas;
		private CanvasScaler rootCanvasScaler;
		private RectTransform rootCanvasRectTransform;

		private void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
			rootCanvas = GetComponentInParent<Canvas>().rootCanvas;
			rootCanvasScaler = GetComponentInParent<CanvasScaler>();
			rootCanvasRectTransform = rootCanvas.GetComponent<RectTransform>();
		}

		private void Update()
		{
			if (aspectRect == null || rectTransform == null || rootCanvas == null)
			{
				return;
			}

			var aspectRectWidth = aspectRect.rect.width;
			var rootCanvasWidth = rootCanvasRectTransform.rect.width;

			//var delta = rootCanvasWidth - aspectRectWidth;

			// 1000 = 1
			// 800 = x

			// x = rootCanvasWidth / (rootCanvasWidth - rootCanvasWidth - aspectRectWidth))

			//var scale = rootCanvasWidth / (rootCanvasWidth - delta) * 1;
			//var scale = (rootCanvasWidth - delta) * 1 / rootCanvasWidth;
			var scale = aspectRectWidth / rootCanvasWidth;

			var localScale = rectTransform.localScale;
			localScale.x = scale;
			localScale.y = scale;
			rectTransform.localScale = localScale;
		}
	}
}