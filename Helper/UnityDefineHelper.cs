namespace RobinBird.Utilities.Unity.Helper
{
	public static class UnityDefineHelper
	{
		public static bool IsWebGL =>
#if UNITY_WEBGL
			true;
#else
			false;
#endif
	}
}