using UnityEngine;

namespace RobinBird.Utilities.Unity.Helper
{
	public static class ClipboardHelper
	{
		public static void CopyToSystemCopyBuffer(string text)
		{
			GUIUtility.systemCopyBuffer = text;
		}
	}
}