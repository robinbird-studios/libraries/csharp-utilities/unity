using System.Reflection;
using RobinBird.Utilities.Unity.Extensions;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Helper
{
	public class AspectRatioConstantSizeFitter : AspectRatioFitter
	{
		[SerializeField]
		private RectTransform sizeContainer;
		[SerializeField]
		private RectTransform constantContainer;
		[SerializeField]
		private Vector2 constantWidth = new Vector2(1000, 0);

		private DrivenRectTransformTracker tracker;
		private RectTransform aspectRect;
		private RectTransform rootCanvasRectTransform;

		protected override void OnEnable()
		{
			base.OnEnable();

			var field = base.GetType().GetField("m_Tracker", BindingFlags.Instance | BindingFlags.NonPublic);
			if (field != null)
			{
				tracker = (DrivenRectTransformTracker)field.GetValue(this);
			}

			aspectRect = GetComponent<RectTransform>();
			rootCanvasRectTransform = aspectRect.GetRootCanvas().GetComponent<RectTransform>();
			UpdateManually();
		}

		[ContextMenu("UpdateManually")]
		public void UpdateManually()
		{
			OnRectTransformDimensionsChange();
		}

		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();

			if (!IsActive() || !IsComponentValidOnObject())
				return;

			if (sizeContainer == null || aspectRect == null || rootCanvasRectTransform == null || constantContainer == null)
			{
				return;
			}
			tracker.Add(this, sizeContainer, DrivenTransformProperties.Scale);
			tracker.Add(this, sizeContainer, DrivenTransformProperties.Anchors);
			tracker.Add(this, sizeContainer, DrivenTransformProperties.SizeDelta);
			tracker.Add(this, sizeContainer, DrivenTransformProperties.Pivot);
			tracker.Add(this, sizeContainer, DrivenTransformProperties.AnchoredPosition);

			var aspectRectWidth = aspectRect.rect.width;
			var rootCanvasWidth = rootCanvasRectTransform.rect.width;

			var scale = aspectRectWidth / constantWidth.x;

			sizeContainer.pivot = new Vector2(0.5f, 0.5f);
			sizeContainer.SetExpandToParent();

			var localScale = sizeContainer.localScale;
			localScale.x = scale;
			localScale.y = scale;
			sizeContainer.localScale = localScale;

			tracker.Add(this, constantContainer, DrivenTransformProperties.SizeDelta);
			tracker.Add(this, constantContainer, DrivenTransformProperties.Anchors);
			tracker.Add(this, constantContainer, DrivenTransformProperties.AnchoredPosition);

			constantContainer.SetAnchorsToCenter();
			constantContainer.offsetMin = Vector2.zero;
			var size = constantContainer.offsetMax;
			size.x = constantWidth.x;
			size.y = constantWidth.x / aspectRatio;
			constantContainer.offsetMax = size;
			constantContainer.anchoredPosition = Vector2.zero;
		}
	}
}