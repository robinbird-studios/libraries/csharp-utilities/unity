using System.Collections;
using RobinBird.Utilities.Unity.Extensions;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Helper
{
	/// <summary>
	/// Lets you specify on a rect transform to extend to the screen edges regardless of the safe area
	/// </summary>
	public class ExtendToScreenEdges : MonoBehaviour
	{
		public bool Top = true;
		public bool Bottom = true;
		public bool Right = true;
		public bool Left = true;

		private IEnumerator Start()
		{
			var canvas = GetComponentInParent<Canvas>();
			while (canvas.enabled == false)
			{
				yield return null;
			}
			ApplyScreenEdgeExtend();
		}

		[ContextMenu("ApplyScreenEdgeExtend")]
		public void ApplyScreenEdgeExtend()
		{
			var rectTransform = GetComponent<RectTransform>();

			if (rectTransform == null || rectTransform.parent == null)
			{
				return;
			}

			Canvas rootCanvas = rectTransform.GetRootCanvas();
			var rootRect = rootCanvas.GetComponent<RectTransform>();

			var rootCorners = new Vector3[4];
			rootRect.GetWorldCorners(rootCorners);

			// for (int i = 0; i < rootCorners.Length; i++)
			// {
			// 	rootCorners[i] = rectTransform.InverseTransformPoint(rootCorners[i]);
			// }

			var parentRect = rectTransform.parent.GetComponent<RectTransform>();
			for (int i = 0; i < rootCorners.Length; i++)
			{
				rootCorners[i] = parentRect.InverseTransformPoint(rootCorners[i]);
			}
			var parentCorners = new Vector3[4];
			parentRect.GetWorldCorners(parentCorners);
			for (int i = 0; i < parentCorners.Length; i++)
			{
				parentCorners[i] = parentRect.InverseTransformPoint(parentCorners[i]);
			}

			//rectTransform.SetExpandToParent();

			if (Top)
				rectTransform.SetTop(parentCorners[1].y - rootCorners[1].y);
			if (Bottom)
				rectTransform.SetBottom(rootCorners[0].y - parentCorners[0].y);
			if (Right)
				rectTransform.SetRight(parentCorners[3].x - rootCorners[3].x);
			if (Left)
				rectTransform.SetLeft(rootCorners[0].x - parentCorners[0].x);
		}
	}
}