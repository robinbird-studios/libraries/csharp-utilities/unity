using System;
using System.Collections.Generic;
using RobinBird.Utilities.Unity.Extensions;
using UnityEditor;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Inspectors
{
#if ROBIN_BIRD_EDITOR_UTILS
	[CustomEditor(typeof (Animation))]
#endif
	public class AnimationInspector : GenericInspector<Animation>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			EditorGUILayout.BeginVertical(GUI.skin.box);
			{
				var inspectorWidth = EditorGUIUtility.currentViewWidth;
				float nameFieldWidth = inspectorWidth * 0.5f;
				float timeWidth = inspectorWidth * 0.3f;

				foreach (AnimationState animationState in Target)
				{
					bool isQueued = animationState.IsQueued();
					if (animationState.enabled == false && isQueued == false)
						continue;
					EditorGUILayout.BeginHorizontal();

					EditorGUILayout.BeginVertical(GUILayout.Width(nameFieldWidth));
					EditorGUILayout.LabelField(animationState.name);
					EditorGUILayout.EndVertical();

					EditorGUILayout.BeginVertical(GUILayout.Width(timeWidth));
					EditorGUILayout.LabelField(isQueued ? "Queued" : "Running");
					EditorGUILayout.LabelField($"Playtime: {animationState.time:F}s");
					EditorGUILayout.LabelField($"Length:   {animationState.length}s");
					if (Math.Abs(animationState.speed - 1) > Mathf.Epsilon)
					{
						EditorGUILayout.LabelField($"Speed: {animationState.speed}x");
					}
					EditorGUILayout.EndVertical();


					EditorGUILayout.EndHorizontal();
				}
			}
			EditorGUILayout.EndVertical();
		}

		public override bool RequiresConstantRepaint()
		{
			return Application.isPlaying && Target.isPlaying;
		}
	}
}