using System;
using RobinBird.Logging.Runtime;
using UnityEditor;
using UnityEditor.U2D;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Extensions
{
	public static class Texture2DExtensions
	{
		public class ReadableTextureHandle : IDisposable
		{
			private readonly bool isReadable;
			private readonly TextureImporter textureImporter;
			//private readonly SpriteAtlasImporter textureImporter;

			public ReadableTextureHandle(Texture2D originalTexture)
			{
				// Get pixel data
				isReadable = originalTexture.isReadable;
				try
				{
					// Ensure original texture is readable
					if (!isReadable)
					{
						var origTexPath = AssetDatabase.GetAssetPath(originalTexture);
						var importer = AssetImporter.GetAtPath(origTexPath);
						if (importer is TextureImporter ti)
						{
							this.textureImporter = ti;
							this.textureImporter.isReadable = true;
							this.textureImporter.SaveAndReimport();
						}
						else
						{
							throw new NotImplementedException($"Could not process asset with importer: {importer}");
						}
					}
				}
				catch (Exception ex)
				{
					Log.Error($"Problem converting texture to readable: {ex}");
				}
			}

			public void Dispose()
			{
				if (!isReadable && textureImporter != null)
				{
					textureImporter.isReadable = false;
					textureImporter.SaveAndReimport();
				}
			}
		}

		public static ReadableTextureHandle SetReadableDisposable(this Texture2D texture2D)
		{
			return new ReadableTextureHandle(texture2D);
		}
	}
}