using System;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Helper;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Editor.Misc
{
    /// <summary>
    /// Property drawer for SerializableGuid
    ///
    /// Author: Searous
    /// </summary>
    [CustomPropertyDrawer(typeof(SerializableGuid))]
    public class SerializableGuidPropertyDrawer : PropertyDrawer
    {
        private const int GuidByteSize = 16;

        public override VisualElement CreatePropertyGUI(SerializedProperty property)
        {
            var container = new IMGUIContainer(() =>
            {
                var screenRect = new Rect(0, 0, 300, EditorGUIUtility.singleLineHeight);
                GUILayout.BeginArea(screenRect);
                DrawProperty(screenRect, property, new GUIContent(property.displayName, property.tooltip));
                GUILayout.EndArea();
            });
            container.style.height = EditorGUIUtility.singleLineHeight;
            return container;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // Start property draw
            DrawProperty(position, property, label);
        }

        public static void DrawProperty(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            var fieldRect = EditorGUI.PrefixLabel(position, label);

            Guid guid = GetGuidFromProperty(property);

            var guidString = guid.ToString("N");
            var newGuidString = EditorGUI.TextField(fieldRect, guidString);
            if (newGuidString != guidString)
            {
                if (Guid.TryParse(newGuidString, out var newGuid))
                {
                    ApplyGuidToProperty(property, newGuid);
                }
            }

            EditorGUI.EndProperty();
        }

        public static Guid GetGuidFromProperty(SerializedProperty serializedProperty)
        {
            var stringProp = serializedProperty.FindPropertyRelative("serializedString");

            if (Guid.TryParseExact(stringProp.stringValue, "N", out Guid result))
            {
                return result;
            }

            return Guid.Empty;

            // SerializedProperty byteArray = serializedProperty.FindPropertyRelative("serializedGuid");
            //
            // var bytes = new byte[GuidByteSize];
            //
            // for (int i = 0; i < GuidByteSize; i++)
            // {
            //     var byteElement = byteArray.GetArrayElementAtIndex(i);
            //     bytes[i] = (byte)byteElement.intValue;
            // }
            //
            // var guid = new Guid(bytes);
            // return guid;
        }

        public static void ApplyGuidToProperty(SerializedProperty property, Guid newGuid)
        {
            var stringProp = property.FindPropertyRelative("serializedString");
            stringProp.stringValue = newGuid.ToString("N");
            // SerializedProperty byteArray = property.FindPropertyRelative("serializedGuid");
            // var newByteArray = newGuid.ToByteArray();
            //
            // for (int i = 0; i < GuidByteSize; i++)
            // {
            //     var byteElement = byteArray.GetArrayElementAtIndex(i);
            //     byteElement.intValue = newByteArray[i];
            // }
        }
    }
}