using RobinBird.Utilities.Unity.Helper;
using UnityEditor;
using UnityEditor.UI;

namespace RobinBird.Utilities.Unity.Editor.Helper
{
	[CustomEditor(typeof(AspectRatioConstantSizeFitter))]
	public class AspectRatioConstantSizeFitterInspector : AspectRatioFitterEditor
	{
		private SerializedProperty sizeContainer;
		private SerializedProperty constantContainer;
		private SerializedProperty constantWidth;

		protected override void OnEnable()
		{
			base.OnEnable();

			sizeContainer = serializedObject.FindProperty("sizeContainer");
			constantContainer = serializedObject.FindProperty("constantContainer");
			constantWidth = serializedObject.FindProperty("constantWidth");
		}

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			serializedObject.Update();

			EditorGUILayout.PropertyField(sizeContainer);
			EditorGUILayout.PropertyField(constantContainer);
			EditorGUILayout.PropertyField(constantWidth);

			serializedObject.ApplyModifiedProperties();
		}
	}
}