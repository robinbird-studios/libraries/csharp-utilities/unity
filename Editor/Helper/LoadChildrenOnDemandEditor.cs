using System.Collections.Generic;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Unity.Editor.Build;
using RobinBird.Utilities.Unity.Editor.Inspectors;
using RobinBird.Utilities.Unity.Extensions;
using RobinBird.Utilities.Unity.Helper;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Editor.Helper
{
	[CustomEditor(typeof(LoadChildrenOnDemand))]
	public class LoadChildrenOnDemandEditor : GenericInspector<LoadChildrenOnDemand>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();
			
			var guidProperty = serializedObject.FindProperty(nameof(LoadChildrenOnDemand.GeneratedPrefabDirectoryGuid));

			var newGuid = EditorTools.DirectoryGuidField(new GUIContent(guidProperty.displayName, guidProperty.tooltip), guidProperty.stringValue);

			if (newGuid != guidProperty.stringValue)
			{
				guidProperty.stringValue = newGuid;
				serializedObject.ApplyModifiedProperties();
			}
			
			var addressableGuid = serializedObject.FindProperty(nameof(LoadChildrenOnDemand.AddressableGroupGuid));

			var newAddressableGuid = EditorTools.ProjectItemGuidField<AddressableAssetGroup>(new GUIContent(addressableGuid.displayName, addressableGuid.tooltip), addressableGuid.stringValue);

			if (newAddressableGuid != addressableGuid.stringValue)
			{
				addressableGuid.stringValue = newAddressableGuid;
				serializedObject.ApplyModifiedProperties();
			}
		}

		[PostProcessScene]
		public static void OnPostProcessScene()
		{
			var views = Object.FindObjectsByType<LoadChildrenOnDemand>(FindObjectsInactive.Include,
				FindObjectsSortMode.None);

			foreach (LoadChildrenOnDemand view in views)
			{
				ExtractLoadPrefabFromView(view, true);
			}
		}

		public static void ExtractLoadPrefabFromView(LoadChildrenOnDemand view, bool destroyOriginal)
		{
			if (view.transform.childCount == 0)
			{
				return;
			}
			
			if (string.IsNullOrEmpty(view.GeneratedPrefabDirectoryGuid))
			{
				Log.Error("Could not extract prefab because directory is empty.");
				return;
			}
			
			if (view.TryGetComponent(out RectTransform rectTransform))
			{
				view.RectTransformAnchoredPosition3d = rectTransform.anchoredPosition3D;
			}

			var dirPath = AssetDatabase.GUIDToAssetPath(view.GeneratedPrefabDirectoryGuid);
			var assetPath = $"{dirPath}/{view.gameObject.name}_ChildrenLoad.prefab";

			var removeImages = view.gameObject.GetComponentsInChildren<RemoveImageSpriteReferenceInBuild>();
			foreach (var removeImage in removeImages)
			{
				removeImage.RemoveSprite();
			}

			var clone = Instantiate(view.gameObject);

			var components = new List<Component>();
			clone.GetComponents(components);
			components.Reverse();

			foreach (Component component in components)
			{
				if (component is RectTransform || component is Transform)
				{
					continue;
				}
				DestroyImmediate(component);
			}
			var prefab = PrefabUtility.SaveAsPrefabAsset(clone, assetPath);
			DestroyImmediate(clone);
			if (destroyOriginal)
			{
				view.gameObject.transform.DestroyChilds();
			}

			string prefabGuid = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(prefab));
			view.PrefabAssetReference = new AssetReferenceGameObject(prefabGuid);

			AddressableAssetSettings defaultSetting = AddressableAssetSettingsDefaultObject.Settings;
			if (string.IsNullOrEmpty(view.AddressableGroupGuid) == false)
			{
				var group = AssetDatabase.LoadAssetAtPath<AddressableAssetGroup>(
					AssetDatabase.GUIDToAssetPath(view.AddressableGroupGuid));

				defaultSetting.CreateOrMoveEntry(prefabGuid, group);
			}
		}
	}
}