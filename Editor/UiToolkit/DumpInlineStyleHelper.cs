using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Extensions;
using UnityEditor;
using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Editor.UiToolkit
{
	public static class DumpInlineStyleHelper
	{
		[MenuItem("Tools/UI Toolkit/Dump Debugger Inline Style")]
		public static void DumpDebuggerSelectedInlineStyle()
		{
			var uiToolkitDebuggerType =
				AppDomain.CurrentDomain.GetType("UnityEditor.UIElements.Debugger.UIElementsDebugger");
			var window = EditorWindow.GetWindow(uiToolkitDebuggerType);

			var debuggerField = uiToolkitDebuggerType.GetField("m_DebuggerImpl", BindingFlags.NonPublic | BindingFlags.Instance);

			var debugger = debuggerField.GetValue(window);

			var styleDebuggerField = debugger.GetType()
				.GetField("m_StylesDebuggerContainer", BindingFlags.Instance | BindingFlags.NonPublic);

			var styleDebugger = styleDebuggerField.GetValue(debugger);

			var selectedElementField = styleDebugger.GetType()
				.GetProperty("selectedElement", BindingFlags.NonPublic | BindingFlags.Instance);

			var selectedElement = (VisualElement)selectedElementField.GetValue(styleDebugger);

			var style = selectedElement.style;

			var changedValuesField =
				style.GetType().GetField("m_Values", BindingFlags.NonPublic | BindingFlags.Instance);

			var changedValues = (IList)changedValuesField.GetValue(style);

			var idToNameMappingField =
				AppDomain.CurrentDomain.GetType("UnityEngine.UIElements.StyleSheets.StylePropertyUtil").GetField("s_IdToName", BindingFlags.Static | BindingFlags.NonPublic);

			var idToNameMapping = idToNameMappingField.GetValue(null);

			var property = idToNameMapping.GetType().GetProperty("Item");

			foreach (object changedValue in changedValues)
			{
				FieldInfo idField = changedValue.GetType().GetField("id", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo numberField = changedValue.GetType().GetField("number", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo lengthField = changedValue.GetType().GetField("length", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo colorField = changedValue.GetType().GetField("color", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo resourceField = changedValue.GetType().GetField("resource", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo positionField = changedValue.GetType().GetField("position", BindingFlags.Instance | BindingFlags.Public);
				FieldInfo repeatField = changedValue.GetType().GetField("repeat", BindingFlags.Instance | BindingFlags.Public);

				var id = idField.GetValue(changedValue);
				var idText = property.GetValue(idToNameMapping, new[] { id });

				var number = numberField.GetValue(changedValue);
				var length = lengthField.GetValue(changedValue);
				var color = colorField.GetValue(changedValue);
				var resource = resourceField.GetValue(changedValue);
				var position = positionField.GetValue(changedValue);
				var repeat = repeatField.GetValue(changedValue);


				Log.Info($"Got id: {id}({idText}) number:{number} length:{length} color: {color} resource:{resource} position:{position} repeat:{repeat}");
			}
		}
	}
}