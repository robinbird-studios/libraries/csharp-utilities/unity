using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace RobinBird.Utilities.Unity.Editor.Localization
{
	public class AutoLocalizedComponentHelper
	{
		[MenuItem("CONTEXT/TextMeshProUGUI/AutoLocalize")]
		static void LocalizeTMProText(MenuCommand command)
		{
			var target = command.context as TextMeshProUGUI;
			SetupForLocalization(target);
		}

		public static MonoBehaviour SetupForLocalization(TextMeshProUGUI target)
		{
			var comp = Undo.AddComponent(target.gameObject, typeof(AutoLocalizeComponent)) as AutoLocalizeComponent;
			var setStringMethod = target.GetType().GetProperty("text").GetSetMethod();
			var methodDelegate = System.Delegate.CreateDelegate(typeof(UnityAction<string>), target, setStringMethod) as UnityAction<string>;
			UnityEditor.Events.UnityEventTools.AddPersistentListener(comp.OnUpdateString, methodDelegate);
			comp.OnUpdateString.SetPersistentListenerState(0, UnityEventCallState.EditorAndRuntime);
			return comp;
		}
	}
}