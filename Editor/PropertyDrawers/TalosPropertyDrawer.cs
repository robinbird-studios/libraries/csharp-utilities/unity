using System;
using System.Collections.Generic;
using System.Reflection;
using RobinBird.Utilities.Unity.PropertyDrawerAttributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Editor.PropertyDrawers
{
	public abstract class TalosPropertyDrawer : PropertyDrawer
	{
		private static Dictionary<Type, Type> propertyAttributeToDrawerMapping;
		
		
		public sealed override VisualElement CreatePropertyGUI(SerializedProperty property)
		{
			if (propertyAttributeToDrawerMapping == null)
			{
				propertyAttributeToDrawerMapping = new();
				var drawers = TypeCache.GetTypesDerivedFrom<TalosPropertyDrawer>();

				foreach (Type drawer in drawers)
				{
					var drawerAttributes = drawer.GetCustomAttributes(typeof(CustomPropertyDrawer), false);
					if (drawerAttributes.Length > 0)
					{
						var drawerAttribute = (CustomPropertyDrawer)drawerAttributes[0];
						var typeField = drawerAttribute.GetType().GetField("m_Type", BindingFlags.Instance | BindingFlags.NonPublic);
						var type = typeField.GetValue(drawerAttribute) as Type;

						if (type != null)
						{
							propertyAttributeToDrawerMapping[type] = drawer;
						}
					}
				}
			}

			VisualElement result = new PropertyField(property);
			foreach (TalosPropertyAttribute customAttribute in fieldInfo.GetCustomAttributes(typeof(TalosPropertyAttribute), false))
			{
				var drawerType = propertyAttributeToDrawerMapping[customAttribute.GetType()];
				var drawerInstance = Activator.CreateInstance(drawerType) as TalosPropertyDrawer;
				typeof(PropertyDrawer).GetField("m_Attribute", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(drawerInstance, customAttribute);
				typeof(PropertyDrawer).GetField("m_FieldInfo", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(drawerInstance, fieldInfo);
				typeof(PropertyDrawer).GetField("m_PreferredLabel", BindingFlags.Instance | BindingFlags.NonPublic).SetValue(drawerInstance, preferredLabel);
				result = drawerInstance.CreateTalosPropertyGUI(property, result);
			}

			return result;
		}

		protected abstract VisualElement CreateTalosPropertyGUI(SerializedProperty property,
			VisualElement currentElement);
	}
}