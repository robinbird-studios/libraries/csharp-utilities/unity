using RobinBird.Utilities.Unity.PropertyDrawerAttributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Editor.PropertyDrawers
{
	[CustomPropertyDrawer(typeof(ColorHighlightAttribute))]
	public class ColorHighlightPropertyDrawer : TalosPropertyDrawer
	{
		protected override VisualElement CreateTalosPropertyGUI(SerializedProperty property,
			VisualElement currentElement)
		{
			var att = (ColorHighlightAttribute)attribute;
			currentElement.style.backgroundColor = att.HighlightColor;
			return currentElement;
		}
	}
}