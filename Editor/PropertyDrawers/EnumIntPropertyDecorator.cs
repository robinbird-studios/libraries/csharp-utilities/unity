using System;
using System.Reflection;
using RobinBird.Utilities.Unity.PropertyDrawerAttributes;
using UnityEditor;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Decorators
{
	[CustomPropertyDrawer(typeof(EnumIntPropertyDrawerAttribute))]
	public class EnumIntPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var att = attribute as EnumIntPropertyDrawerAttribute;

			var currentEnum = (Enum)Enum.ToObject(att.EnumType, property.intValue);

			var newValue = EditorGUI.EnumPopup(position, currentEnum);

			if (!Equals(newValue, currentEnum))
			{
				property.intValue = Convert.ToInt32(newValue);
				property.serializedObject.ApplyModifiedProperties();
			}
		}
	}
}