using RobinBird.Utilities.Unity.PropertyDrawerAttributes;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Editor.PropertyDrawers
{
	[CustomPropertyDrawer(typeof(BoldLabelAttribute))]
	public class BoldLabelPropertyDrawer : TalosPropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property,
			GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property, label, true);
		}

		protected override VisualElement CreateTalosPropertyGUI(SerializedProperty property,
			VisualElement currentElement)
		{
			currentElement.style.unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold);
			return currentElement;
		}

		public override void OnGUI(Rect position,
			SerializedProperty property,
			GUIContent label)
		{
			var origFontStyle = EditorStyles.label.fontStyle;
			EditorStyles.label.fontStyle = FontStyle.Bold;
			EditorGUI.PropertyField(position, property, label, true);
			EditorStyles.label.fontStyle = origFontStyle;
		}
	}
}