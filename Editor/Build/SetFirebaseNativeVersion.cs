using System.IO;
using RobinBird.Logging.Runtime;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Build
{
	[CreateAssetMenu(fileName = nameof(SetFirebaseNativeVersion), menuName = CreateAssetMenuItemName + nameof(SetFirebaseNativeVersion))]
	public class SetFirebaseNativeVersion : AbstractCIBuildCallbackHandler
	{
		[SerializeField]
		private string currentVersion;

		[SerializeField]
		private string newVersion;
		
		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			if (target == BuildTarget.iOS)
			{
				var podFilePath = Path.Combine(buildPath, "Podfile");

				var podFileText = File.ReadAllText(podFilePath);

				podFileText = podFileText.Replace(currentVersion, newVersion);
				
				File.WriteAllText(podFilePath, podFileText);
			}
			else if (target == BuildTarget.Android)
			{
				Log.Warn("Currently not supported");
			}

			return true;
		}
	}
}