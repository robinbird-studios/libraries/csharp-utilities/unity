using System.IO;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Unity.Editor.Helper;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Build
{
	[CreateAssetMenu(fileName = nameof(CopyFilesToBuildOutput), menuName = CreateAssetMenuItemName + nameof(CopyFilesToBuildOutput))]
	public class CopyFilesToBuildOutput : AbstractCIBuildCallbackHandler
	{
		public Object[] FilesToCopy;
		
		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			foreach (Object fileToCopy in FilesToCopy)
			{
				var filePath = AssetDatabase.GetAssetPath(fileToCopy);

				string sourcePath = Path.Combine(EditorTools.GetProjectPath(), filePath);
				string targetPath = Path.Combine(buildPath, Path.GetFileName(filePath));
				Log.Info($"Copy from {sourcePath} to {targetPath}");
				File.Copy(sourcePath, targetPath, true);
			}
			return true;
		}
	}
}