using System;
using System.IO;
using System.Reflection;
using RobinBird.Utilities.Unity.Configuration;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.Initialization;

namespace RobinBird.Utilities.Unity.Editor.Build.Web
{
	[CreateAssetMenu(fileName = nameof(AddressableCacheBreaker), menuName = WebCreateAssetMenuItemName + nameof(AddressableCacheBreaker))]
	public class AddressableCacheBreaker : AbstractCIBuildCallbackHandler
	{
		//protected override Type[] CallbackDependencies => new[] { typeof(AddressableBuild) };

		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			// var runtimePath = Addressables.BuildPath;
			// var settingsFile = Path.Combine(runtimePath, "settings.json");
			//
			// if (File.Exists(settingsFile))
			// {
			// 	string settingsFileText = File.ReadAllText(settingsFile);
			// 	ResourceManagerRuntimeData settings = JsonUtility.FromJson<ResourceManagerRuntimeData>(settingsFileText);
			//
			// 	var localCatalog = settings.CatalogLocations[0];
			//
			// 	var internalId = typeof(ResourceManagerRuntimeData).GetField("m_InternalId",
			// 		BindingFlags.NonPublic | BindingFlags.Instance);
			// 	internalId.SetValue(settings, $"{localCatalog.InternalId}?cacheBreak={BuildProperties.Default.BuildNumber.ToString()}");
			// 	
			// 	var newFileText = JsonUtility.ToJson(settings, true);
			// 	File.WriteAllText(settingsFile, newFileText);
			// }
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}
	}
}