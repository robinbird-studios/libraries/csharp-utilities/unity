using System.IO;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Helper;
using RobinBird.Utilities.Unity.Editor.Helper;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Build.Android
{
	[CreateAssetMenu(fileName = nameof(RedirectIl2CppOutputToLogFile), menuName = AndroidCreateAssetMenuItemName + nameof(RedirectIl2CppOutputToLogFile))]
	public class RedirectIl2CppOutputToLogFile : AbstractCIBuildCallbackHandler
	{
		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			if (target != BuildTarget.Android)
			{
				Log.Warn("This post process step is only suited for Android");
				return true;
			}
			var unityLibraryGradleFilePath = Path.Combine(buildPath, "unityLibrary", "build.gradle");

			if (File.Exists(unityLibraryGradleFilePath) == false)
			{
				Log.Error($"Could not find gradle build file for unityLibrary at: {unityLibraryGradleFilePath}");
				return false;
			}

			var text = File.ReadAllText(unityLibraryGradleFilePath);

			var projectDir = EditorTools.GetProjectPath();
			var relativePath = PathHelper.MakeRelativePath(buildPath, projectDir);
			Log.Info($"Extracted relative path from build to project: {relativePath}");

			// Adding one more directory up because gradle is operating from within the buildPath directory
			var newText = text.Replace("args commandLineArgs", "args commandLineArgs\n        standardOutput new FileOutputStream(\"${project.projectDir}/../../../logs/il2cpp.log\")");

			if (text == newText)
			{
				Log.Error("Could not find relative path to maven repository");
				return false;
			}

			File.WriteAllText(unityLibraryGradleFilePath, newText);

			return true;
		}
	}
}