using System.IO;
using System.Text;
using RobinBird.Logging.Runtime;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Build.Android
{
	[CreateAssetMenu(fileName = nameof(SetupAndroidStudioForLocalBuild), menuName = AndroidCreateAssetMenuItemName + nameof(SetupAndroidStudioForLocalBuild))]
	public class SetupAndroidStudioForLocalBuild : AbstractCIBuildCallbackHandler
	{
		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions,
			string buildPath)
		{
			if (target != BuildTarget.Android)
			{
				Log.Warn("This post process step is only suited for Android");
				return true;
			}

			var buildGradleFilePath = Path.Combine(buildPath, "launcher", "build.gradle");

			string gradleFileText = File.ReadAllText(buildGradleFilePath);

			if (gradleFileText.Contains("getPassword(String opSecretReference)"))
			{
				// This is probably an incremental build. Not replacing anything
				return true;
			}
			
			var sb = new StringBuilder(gradleFileText);


			// TODO: Replace with project name
			sb.Replace("storePassword ''",
				"storePassword getPassword(\"op://RobinBirdDevOps/BrewTycoon Debug KeyStore/password\")");
			sb.Replace("keyPassword ''",
				"keyPassword getPassword(\"op://RobinBirdDevOps/BrewTycoon Debug KeyStore/Key/password\")");

			
			sb.AppendLine();
			sb.AppendLine(@"def getPassword(String opSecretReference) {
	def stdout = new ByteArrayOutputStream()
	def stderr = new ByteArrayOutputStream()
	println(""Getting password for reference: $opSecretReference"")
    exec {
        commandLine 'which', 'op'
        standardOutput = stdout
        errorOutput = stderr
        ignoreExitValue true
    }
    def opPath = stdout.toString().trim()
    println(""Got op path: $opPath"")
    def stdout2 = new ByteArrayOutputStream()
    def stderr2 = new ByteArrayOutputStream()
	exec {
	  commandLine opPath, 'read', '--no-newline', opSecretReference
	  standardOutput = stdout2
	  errorOutput = stderr2
	  ignoreExitValue true
	}
	//noinspection GroovyAssignabilityCheck
	def password = stdout2.toString().trim()
	//println(""Got password: $password"")
	password
}");

			File.WriteAllText(buildGradleFilePath, sb.ToString());
			return true;
		}
	}
}