using System.IO;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Helper;
using RobinBird.Utilities.Unity.Editor.Helper;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace RobinBird.Utilities.Unity.Editor.Build.Android
{
	[CreateAssetMenu(fileName = nameof(MakeGradleProjectPathRelative), menuName = AndroidCreateAssetMenuItemName + nameof(MakeGradleProjectPathRelative))]
	public class MakeGradleProjectPathRelative : AbstractCIBuildCallbackHandler
	{
		public override bool OnPreBuild(BuildTarget target, BuildOptions buildOptions, string buildPath)
		{
			return true;
		}

		public override bool OnPostBuild(BuildReport report, BuildTarget target, BuildOptions buildOptions,
			string buildPath)
		{
			if (target != BuildTarget.Android)
			{
				Log.Warn("This post process step is only suited for Android");
				return true;
			}
			var gradleSettingsFilePath = Path.Combine(buildPath, "settings.gradle");

			if (File.Exists(gradleSettingsFilePath) == false)
			{
				Log.Error($"Could not find gradle settings file at: {gradleSettingsFilePath}");
				return false;
			}

			var text = File.ReadAllText(gradleSettingsFilePath);

			var projectDir = EditorTools.GetProjectPath();
			var relativePath = PathHelper.MakeRelativePath(buildPath, projectDir);
			Log.Info($"Extracted relative path from build to project: {relativePath}");

			// Adding one more directory up because gradle is operating from within the buildPath directory
			var newText = text.Replace($"\"file:///{projectDir}", $"\"file:///${{project(':unityLibrary').projectDir}}/../../{relativePath}");

			if (text == newText)
			{
				Log.Error("Could not find relative path to maven repository");
				return false;
			}

			File.WriteAllText(gradleSettingsFilePath, newText);

			return true;
		}
	}
}