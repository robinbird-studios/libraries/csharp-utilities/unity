using UnityEngine.UIElements;

namespace RobinBird.Utilities.Unity.Extensions
{
	public static class VisualElementExtensions
	{
		public static VisualElement GetFirstAncestorWithClass(this VisualElement element, string className)
		{
			if (element == null)
				return null;

			if (element.ClassListContains(className))
				return element;

			return element.parent.GetFirstAncestorWithClass(className);
		}
	}
}