using System;
using System.Collections;
using System.Threading.Tasks;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Helper;
using RobinBird.Utilities.Unity.Helper;

namespace RobinBird.Utilities.Unity.Extensions
{
	public class CoroutineTask
	{
		public Func<CoroutineTask, IEnumerator> Iterator;
		public bool IsCanceled {get; set;}
		public bool IsFaulted {get; set;}
		public Exception Exception {get; set;}
		public bool IsCompleted {get; set;}

		public void Complete()
		{
			IsCompleted = true;
		}
	}
	
	public class CoroutineTask<T> : CoroutineTask
	{
		public new Func<CoroutineTask<T>, IEnumerator> Iterator;
		public T Result {get; private set;}

		[Obsolete("You should always set an result", true)]
		public new void Complete()
		{
			IsCompleted = true;
		}
		public void Complete(T result)
		{
			Result = result;
			IsCompleted = true;
		}
	}
	
	public static class TaskExtensions
	{
#if ROBIN_BIRD_FIREBASE 
#if !UNITY_EDITOR && UNITY_WEBGL
		private static void InternalContinue(CoroutineTask task, Func<CoroutineTask, IEnumerator> iterator, string errorScope, Action<CoroutineTask> continuation, Action onError)
		{
			IEnumerator ContinueCoroutine(CoroutineTask routineTask)
			{
				Log.Info("Invoking routine");
				yield return iterator.Invoke(routineTask);
				Log.Info("Finished routine");
				if (routineTask.IsCompleted == false)
				{
					ErrorTracking.RecordException("UncompletedTaskAfterContinue", "Tasks must be completed within the Coroutine");
				}
				Log.Info("Calling continue");
				continuation(routineTask);
				HandleDoneTask(routineTask);
			}

			
			if (task.IsCompleted)
			{
				continuation(task);
				HandleDoneTask(task);
			}
			else
			{
				Log.Info("Starting routine");
				if (iterator == null)
				{
					Log.Error("Iterator null!");
					continuation(task);
					onError?.Invoke();
				}
				else
				{
					RoutineDispatcher.Instance.StartCoroutine(ContinueCoroutine(task));
				}
			}

			void HandleDoneTask(CoroutineTask doneTask)
			{
				if (doneTask.IsCanceled || doneTask.IsFaulted)
				{
					ErrorTracking.RecordException($"{errorScope}TaskCoroutineOperationFailed", innerException: doneTask.Exception);
					onError?.Invoke();
				}
			}
		}

		public static void ContinueWithErrorHandlingOnMainThread(this CoroutineTask task, string errorScope,
			Action<CoroutineTask> continuation, Action onError = null)
		{
			InternalContinue(task, task.Iterator, errorScope, continuation, onError);
		}

		public static void ContinueWithErrorHandlingOnMainThread<T>(this CoroutineTask<T> task, string errorScope,
			Action<CoroutineTask<T>> continuation, Action onError = null)
		{
			InternalContinue(task, coroutineTask =>
			{
				var localTask = (CoroutineTask<T>)coroutineTask;
				return localTask.Iterator(localTask);
			}, errorScope, coroutineTask =>
			{
				var localTask = (CoroutineTask<T>)coroutineTask;
				continuation(localTask);
			}, onError);
			// ContinueWithErrorHandlingOnMainThread((CoroutineTask)task, errorScope,
			// 	coroutineTask => continuation((CoroutineTask<T>)coroutineTask), onError);
		}
#else
		/// <summary>
		/// Queue handler on main thread
		/// </summary>
		public static void ContinueWithErrorHandlingOnMainThread(this Task task, Action<Task> continuation, Action onError = null)
		{
			Firebase.Extensions.TaskExtension.ContinueWithOnMainThread(task, FinishTask);

			void FinishTask(Task finishedTask)
			{
				try
				{
					continuation(finishedTask);
					if (finishedTask.IsCanceled || finishedTask.IsFaulted)
					{
						ErrorTracking.RecordException($"AsyncOperationFailed", innerException: finishedTask.Exception);
						onError?.Invoke();
					}
				}
				catch (Exception e)
				{
					ErrorTracking.RecordException($"AsyncOperationException", innerException: e);
					onError?.Invoke();
				}
			}
		}

		/// <summary>
		/// Queue handler on main thread with error handling
		/// </summary>
		/// <param name="errorScope">UpperCaseCamelCase - Define a scope so that not all async errors are grouped in one exception on crashltyics</param>
		public static void ContinueWithErrorHandlingOnMainThread(this Task task, string errorScope, Action<Task> continuation, Action onError = null)
		{
			Firebase.Extensions.TaskExtension.ContinueWithOnMainThread(task, FinishTask);

			void FinishTask(Task finishedTask)
			{
				try
				{
					continuation(finishedTask);
					if (finishedTask.IsCanceled || finishedTask.IsFaulted)
					{
						ErrorTracking.RecordException($"{errorScope}AsyncOperationFailed", innerException: finishedTask.Exception);
						onError?.Invoke();
					}
				}
				catch (Exception e)
				{
					ErrorTracking.RecordException($"{errorScope}AsyncOperationException", innerException: e);
					onError?.Invoke();
				}
			}
		}

		public static void ContinueWithErrorHandlingOnMainThread<T>(this Task<T> task, string errorScope, Action<Task<T>> continuation, Action onError = null)
		{
			ContinueWithErrorHandlingOnMainThread((Task)task, errorScope, task1 =>
			{
				continuation((Task<T>)task1);
			}, onError);
		}
#endif
#endif
	}
}