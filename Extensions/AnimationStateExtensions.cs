using UnityEngine;

namespace RobinBird.Utilities.Unity.Extensions
{
	public static class AnimationStateExtensions
	{
		private const string QueuedCloneClipName = "Queued Clone";

		public static bool IsQueued(this AnimationState state)
		{
			return state.enabled == false && state.name.Contains(QueuedCloneClipName);
		}
	}
}