﻿#region Disclaimer
// <copyright file="RechtExtensions.cs">
// Copyright (c) 2018 - 2018 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion
namespace RobinBird.Utilities.Unity.Extensions
{
    using UnityEngine;

    public static class RectExtensions
    {
        public static Rect Shrink(this Rect rect, float shrinkValue)
        {
            return MoveBorders(rect, new Vector2(-shrinkValue, -shrinkValue));
        }

        public static Rect Grow(this Rect rect, float growValue)
        {
            return MoveBorders(rect, new Vector2(growValue, growValue));
        }
        
        /// <summary>
        /// Returns the intersecting rectangle of two given rectangles.
        /// If there is no intersection, an empty rectangle is returned.
        /// </summary>
        public static bool Intersects(this Rect rectA, Rect rectB, out Rect area)
        {
            float x1 = Mathf.Max(rectA.xMin, rectB.xMin);
            float x2 = Mathf.Min(rectA.xMax, rectB.xMax);
            float y1 = Mathf.Max(rectA.yMin, rectB.yMin);
            float y2 = Mathf.Min(rectA.yMax, rectB.yMax);
            // Check if there is an actual intersection
            if (x2 >= x1 && y2 >= y1)
            {
                area = new Rect(x1, y1, x2 - x1, y2 - y1);
                return true;
            }
            // Return an empty rectangle if there is no intersection
            area = Rect.zero;
            return false;
        }

        private static Rect MoveBorders(Rect rect, Vector2 value)
        {
            return new Rect(rect.x - value.x, rect.y - value.y, rect.width + value.x * 2, rect.height + value.y * 2);
        }

        /// <summary>
        /// Returns the start and end points (in world coordinates) of the *nearest edge* of a Rect to a given point.
        /// </summary>
        /// <param name="rect">Axis-aligned Rect in world space.</param>
        /// <param name="point">Point in 2D space.</param>
        /// <returns>A tuple (start, end) that represents the nearest edge’s endpoints.</returns>
        public static (Vector2 start, Vector2 end) GetNearestEdgeSegment(this Rect rect, Vector2 point)
        {
            // 1. Clamp the point so it’s inside or on the boundary of the Rect.
            float clampedX = Mathf.Clamp(point.x, rect.xMin, rect.xMax);
            float clampedY = Mathf.Clamp(point.y, rect.yMin, rect.yMax);

            // Calculate distances from (clampedX, clampedY) to each edge
            float distToLeft   = clampedX - rect.xMin;
            float distToRight  = rect.xMax - clampedX;
            float distToBottom = clampedY - rect.yMin;
            float distToTop    = rect.yMax - clampedY;

            // Determine which edge is closest
            float minDist = Mathf.Min(Mathf.Min(distToLeft, distToRight), Mathf.Min(distToBottom, distToTop));

            // 2. Return the segment for that edge
            if (Mathf.Approximately(minDist, distToLeft))
            {
                // Left edge: from (xMin, yMin) to (xMin, yMax)
                return (new Vector2(rect.xMin, rect.yMin), new Vector2(rect.xMin, rect.yMax));
            }
            else if (Mathf.Approximately(minDist, distToRight))
            {
                // Right edge: from (xMax, yMin) to (xMax, yMax)
                return (new Vector2(rect.xMax, rect.yMin), new Vector2(rect.xMax, rect.yMax));
            }
            else if (Mathf.Approximately(minDist, distToTop))
            {
                // Top edge: from (xMin, yMax) to (xMax, yMax)
                return (new Vector2(rect.xMin, rect.yMax), new Vector2(rect.xMax, rect.yMax));
            }
            else
            {
                // Bottom edge: from (xMin, yMin) to (xMax, yMin)
                return (new Vector2(rect.xMin, rect.yMin), new Vector2(rect.xMax, rect.yMin));
            }
        }
    }
}