using System;
using RobinBird.Logging.Runtime;
using RobinBird.Utilities.Runtime.Helper;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UI;

namespace RobinBird.Utilities.Unity.Extensions
{
	public static class AsyncOperationExtensions
	{
		public static AssetReferenceSprite FallbackSpriteAssetReferenceSprite = new AssetReferenceSprite("6fa80fe75820b41729fa9dc2c20882d9");

	    /// <summary>
        /// If the asset is loaded the callback is fired. If the asset is not loaded the loading will be started and the callback is hooked up
        /// </summary>
        public static AsyncOperationHandle<TResult> ProcessResultWhenReady<TResult>(this AssetReferenceT<TResult> reference, Action<TResult> processAction) where TResult : UnityEngine.Object
        {
	        if (reference == null)
	        {
		        return default;
	        }

	        if (string.IsNullOrEmpty(reference.AssetGUID))
	        {
		        return default;
	        }
            var operation = Addressables.LoadAssetAsync<TResult>(reference);
	        operation.Completed += handle =>
            {
                processAction(handle.Result);
            };
	        return operation;
        }
        
        public static AsyncOperationHandle<Sprite> ProcessResultToImage(this AssetReferenceSprite reference, Image image)
        {
	        if (reference == null)
	        {
		        return default;
	        }

            if (image == null)
            {
                ErrorTracking.RecordException("ImageRefAssignToNull", $"Image is null. Sprite which was assigned: {reference}");
                return default;
            }

            if (reference.RuntimeKey == null || string.IsNullOrEmpty(reference.RuntimeKey.ToString()))
            {
	            ErrorTracking.RecordException("UsedFallbackImage", $"Had to use fallback on image: {image.gameObject.GetScenePath()}");
	            reference = FallbackSpriteAssetReferenceSprite;
            }
            var operation = reference.ProcessResultWhenReady(sprite =>
            {
	            if (image == null)
	            {
		            Log.Warn($"Assigning sprite ({sprite.name}) to destroyed image.");
		            return;
	            }
                image.sprite = sprite;
            });
            return operation;
        }
		
		public static void OnCompleteWithErrorHandling<T>(this AsyncOperationHandle<T> op, Action<AsyncOperationHandle<T>> continueAction)
		{
			op.Completed += handle =>
			{
				if (handle.Status == AsyncOperationStatus.Failed)
				{
					ErrorTracking.RecordException($"{op.DebugName}OperationFailed", $"Exception: {op.OperationException}");
				}

				continueAction(handle);
			};
		}
		
		public static void OnCompleteWithErrorHandling<T>(this AsyncOperationHandle<T> op, Action<AsyncOperationHandle<T>> onSuccess, Action<AsyncOperationHandle<T>> onFailed)
		{
			op.Completed += handle =>
			{
				if (handle.Status == AsyncOperationStatus.Failed)
				{
					ErrorTracking.RecordException($"{op.DebugName}OperationFailed", $"Exception: {op.OperationException}");
					onFailed(handle);
				}
				else
				{
					onSuccess(handle);
				}
			};
		}
	}
}