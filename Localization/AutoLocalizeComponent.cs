using RobinBird.Logging.Runtime;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.Localization.Settings;

namespace RobinBird.Utilities.Unity.Editor.Localization
{
	public class AutoLocalizeComponent : LocalizeStringEvent
	{
		private void OnValidate()
		{
			if (Application.isEditor && Application.isPlaying == false)
			{
				if (StringReference != null && StringReference.TableEntryReference.KeyId != 0)
				{
					string key = StringReference.TableEntryReference.ToString(StringReference.TableReference);
					var dashIndex = key.IndexOf('-');
					key = key.Substring(dashIndex + 2, key.Length - dashIndex - 3);
					UpdateString($"Key.{key}");
				}
			}
		}
	}
}